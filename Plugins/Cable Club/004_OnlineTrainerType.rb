class Player
  attr_writer :online_trainer_type
  def online_trainer_type
    return @online_trainer_type || self.trainer_type
  end
end

module CableClub
  @@onUpdateTrainerType                 = Event.new
  
  # Fires whenever the online_trainer_type is changed
  # Parameters:
  # e[0] - the new online_trainer_type
  def self.onUpdateTrainerType;     @@onUpdateTrainerType;     end
  def self.onUpdateTrainerType=(v); @@onUpdateTrainerType = v; end
end