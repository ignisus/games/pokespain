<h1 align="center">PokeSpain</h1>

<div align="center">

[![RUBY](https://img.shields.io/badge/Ruby-CC342D?style=for-the-badge&logo=ruby&logoColor=white)](https://www.w3.org/TR/2014/REC-html5-20141028/) 
[![GIMP](https://img.shields.io/badge/gimp-5C5543?style=for-the-badge&logo=gimp&logoColor=white)](https://www.w3.org/TR/2001/WD-css3-roadmap-20010523/) 
[![AUDACITY](https://img.shields.io/badge/Audacity-0000CC?style=for-the-badge&logo=audacity&logoColor=white****)](https://developer.mozilla.org/en-US/docs/Web/javascript)

</div>

### Forge from: [Maruno17](https://github.com/Maruno17/pokemon-essentials)

### Releases: [Download](https://gitlab.com/ignisus/games/pokespain/-/releases)

# Linux Instalation

## x86: Use a wine system (WinHQ, Lutris, Proton...)

## arm: Use a wine system (WinHQ, Lutris, Proton...)

## Android: Download from Play Store or Aurora Store EasyRPG Player and run git files.

# MacOs

## Mx: Use a wine system (WinHQ, Lutris, Proton...)

## Intel Use a wine system (WinHQ, Lutris, Proton...)

# Windows

## x86: Execute the .exe

## arm: Not supported yet
